/*
 * @Author: zhanwei xu
 * @Date: 2023-06-28 00:56:41
 * @LastEditors: zhanwei xu
 * @LastEditTime: 2023-06-28 01:23:02
 * @Description:
 *
 * Copyright (c) 2023 by zhanwei xu, Tsinghua University, All Rights Reserved.
 */
import { Site } from "@/config/base";
import { setDocumentTitle } from "@/utils/common.methods";
import { createRouter, createWebHashHistory, createWebHistory, RouteRecordRaw } from "vue-router";

// 2. 定义一些路由
// 每个路由都需要映射到一个组件。
const routes: Readonly<RouteRecordRaw[]> = [
  { name: "index", path: "/", component: () => import("@/Index.vue"), meta: { transitionName: "slide-left", title: Site.name } },
  {
    name: "chat",
    path: "/chat",
    component: () => import("@/views/Chat.vue"),
    meta: { transitionName: "slide-left", title: "chat" },
    children: [
      { path: "working-edition", component: () => import("@/views/WorkingEdition.vue"), meta: { transitionName: "slide-left", title: "高效问答" } },
      // { path: "youth-version", component: () => import("@/views/YouthVersion.vue"), meta: { transitionName: "slide-left", title: "青春版" } },
      { path: "online-search", component: () => import("@/views/OnlineSearch.vue"), meta: { transitionName: "slide-left", title: "在线检索" } },
      { path: "share/:uuid", component: () => import("@/views/Share.vue"), meta: { transitionName: "slide-left", title: "分享" } },
      { path: "aidraw", component: () => import("@/views/Aidraw.vue"), meta: { transitionName: "slide-left", title: "随心作画" } },
      { path: "documentQA", component: () => import("@/views/DocumentQA.vue"), meta: { transitionName: "slide-left", title: "文档分析" } },
    ],
  },
  { name: "login", path: "/login", component: () => import("@/views/user/Login.vue"), meta: { transitionName: "slide-left", title: "登录" } },
];

//  创建路由实例并传递 `routes` 配置
const router = createRouter({
  // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  // history: createWebHistory(),
  history: createWebHashHistory(),
  routes, // `routes: routes` 的缩写
});
// //beforeEach是router的钩子函数，在进入路由前执行
router.beforeEach((to, from, next) => {
  console.log(from);
  //判断是否有标题
  if (to?.meta?.title) {
    setDocumentTitle((to.meta.title + " | " + Site.slogan) as string);
  }
  next(); //执行进入路由，如果不写就不会进入目标页
});

export default router;
// 现在，应用已经启动了！
