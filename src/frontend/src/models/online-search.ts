import { getUuid } from "@/utils/common.methods";
import { SiderMenuEnum } from "./@types";

export type OnlineSearchInfo = {
  uid: string;
  label: string;
  type: SiderMenuEnum;
  database: DatabaseEnum;
  childrens?: OnlineSearchInfo[];
  question?: string;
  answer?: string;
};

export enum DatabaseEnum {
  Default = "default",
  Bing = "bing",
  Arxiv = "arxiv",
}

export const OnlineSearchModelEnumOption = [
  {
    key: DatabaseEnum.Default,
    name: "默认",
  },
  {
    key: DatabaseEnum.Bing,
    name: "Bing",
  },
  {
    key: DatabaseEnum.Arxiv,
    name: "Arxiv",
  },
];

export type LOCAL_STORAGEInfo = {
  currKey?: string;
  menus?: OnlineSearchInfo[];
};

export class OnlineSearchModel {
  private LOCAL_STORAGE_KEY = "online-search";

  private _data = <LOCAL_STORAGEInfo>{};

  private _list: OnlineSearchInfo[] = [];
  private _max: number = 10; // 最大可保存多少条历史记录

  constructor() {}

  public setData(): void {
    const dataStr = localStorage.getItem(this.LOCAL_STORAGE_KEY);
    this._data = dataStr ? JSON.parse(dataStr) : {};
  }
  /** 左侧目录 */
  public get menus(): OnlineSearchInfo[] {
    if (!this._data?.currKey) {
      this.setData();
    }
    return this._data?.menus || [];
  }

  public add(type: SiderMenuEnum, parent?: string): void {
    const label = (type === SiderMenuEnum.Group ? "合集" : "会话") + getUuid(5);
    const uid = getUuid();
    const item: OnlineSearchInfo = { uid, label, type, database: DatabaseEnum.Default };
    if (parent) {
      this._data?.menus?.map((val) => {
        if (val.uid === parent) {
          val.childrens = (val.childrens || []).concat(item);
        }
        return val;
      });
    } else {
      this._data = {
        currKey: uid,
        menus: (this._data?.menus || []).concat(item),
      };
    }
    this.setlocalStorage("add");
  }

  public edit(uid: string, label: string): void {
    this._data.menus?.forEach((menu) => {
      if (menu.uid === uid) menu.label = label;
      menu.childrens?.forEach((child) => {
        if (child.uid === uid) child.label = label;
      });
    });
    this.setlocalStorage("edit");
  }

  public remove(uid: string): void {
    this._data.menus = this._data.menus?.filter((val) => {
      val.childrens = val.childrens?.filter((child) => child.uid !== uid);
      return val.uid !== uid;
    });
    this._data.currKey = this._data.menus[0]?.uid || "";
    this.setlocalStorage("remove");
  }

  public setCurrKey(uid: string): void {
    this._data.currKey = uid;
    this.setlocalStorage("setCurrKey");
  }

  public get currKey(): string {
    return this._data?.currKey;
  }

  public get currMenu(): OnlineSearchInfo {
    if (!this._data?.menus?.length) return;
    const key = this.currKey;
    if (this._data?.menus) {
      for (const item of this._data?.menus) {
        if (item.uid === key) {
          return item;
        } else {
          if (item?.childrens) {
            for (const child of item?.childrens) {
              if (child.uid === key) return child;
            }
          }
        }
      }
    }
    return null;
  }

  // public get list(): OnlineSearchInfo[] {
  //   if (!this._list?.length) {
  //     const dataStr = localStorage.getItem(this.LOCAL_STORAGE_KEY);
  //     this._list = dataStr ? JSON.parse(dataStr) : {};
  //   }
  //   return this._list;
  // }

  /** 修改记录中的 问题及答案 */
  public update(uid: string, question: string, answer: string): void {
    if (this._data.menus) {
      for (const item of this._data.menus) {
        if (item.uid === uid) {
          item.question = question;
          item.answer = answer;
        }
        if (item.childrens) {
          for (const child of item.childrens) {
            if (child.uid === uid) {
              child.question = question;
              child.answer = answer;
            }
          }
        }
      }
    }
    this.setlocalStorage("add");
  }

  // public item(uid?: string): OnlineSearchInfo {
  //   if (uid) {
  //     return this.list?.find((val) => val.uid === uid);
  //   }
  //   return this.list[0];
  // }

  /** 清空 */
  public clear(): void {
    this._data = {};
    this.setlocalStorage("clear");
  }

  private setlocalStorage(soure: string): void {
    console.log("setlocalStoragesetloca >>> soure :", soure, this._data);
    localStorage.setItem(this.LOCAL_STORAGE_KEY, JSON.stringify(this._data));
  }
}
