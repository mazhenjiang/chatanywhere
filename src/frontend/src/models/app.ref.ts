import { Ref, ref } from "vue";
import { ChatFileModel } from "./chat-file";
import { ChatWorkModel } from "./chat-work";
import { UserModel } from "./user";
import { AIDrawModel } from "./aidraw";
import { OnlineSearchModel } from "./online-search";
import { ThemeEnum } from "./app.context";

class AppRef {
  private static _ins: Ref = ref(new AppRef());

  public theme: ThemeEnum = ThemeEnum.Dark;

  private _user: UserModel = new UserModel();
  private _chatFile: ChatFileModel = new ChatFileModel();
  private _aidraw: AIDrawModel = new AIDrawModel();
  private _chatWork: ChatWorkModel = new ChatWorkModel();
  private _onlineSearch: OnlineSearchModel = new OnlineSearchModel();

  constructor() {}

  public static get ins(): AppRef {
    return this._ins?.value ? this._ins.value : (this._ins.value = new AppRef());
  }

  /** 用户类 */
  public get user(): UserModel {
    return this._user;
  }

  /** 文档分析 */
  public get chatFile(): ChatFileModel {
    return this._chatFile;
  }

  /** AI 绘画 */
  public get aidraw(): AIDrawModel {
    return this._aidraw;
  }

  /** 高效工作 */
  public get chatWork(): ChatWorkModel {
    return this._chatWork;
  }

  /** 联网搜索 */
  public get onlineSearch(): OnlineSearchModel {
    return this._onlineSearch;
  }
}
/** 全局响应式 状态管理池 */
export const appRef: AppRef = AppRef.ins;
