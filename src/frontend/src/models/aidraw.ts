import { getUuid } from "@/utils/common.methods";
import { AessionInfo, SiderMenuEnum } from "./@types";
import { Message } from "@arco-design/web-vue";

export enum DrawModelEnum {
  Normal = 'normal',
  Structure = 'structure',
  Anime = 'anime',
}

export const DrawModelEnumOpention = [
  {
    key: DrawModelEnum.Normal,
    name: '通用模型'
  },
  {
    key: DrawModelEnum.Structure,
    name: '建筑设计模型'
  },
  {
    key: DrawModelEnum.Anime,
    name: '二次元模型'
  },
]

export type AIDrawMenuImageInfo = {
  uid: string;
  task_id: string;
  img: string;
  forward: string;
  exclude?: string;
  model: DrawModelEnum;
}

export type AIDrawMenuInfo = {
  uid: string;
  label: string;
  type: SiderMenuEnum;
  image?: AIDrawMenuImageInfo
  childrens?: AIDrawMenuInfo[];
};

export type ChatFileInfo = {
  currKey?: string;
  menus?: AIDrawMenuInfo[];
};

export class AIDrawModel {
  private LOCAL_STORAGE_KEY = "chat-aidraw";
  private _data = <ChatFileInfo>{};

  constructor() { }

  public setData(): void {
    const dataStr = localStorage.getItem(this.LOCAL_STORAGE_KEY);
    this._data = dataStr ? JSON.parse(dataStr) : {};
  }

  /** 左侧目录 */
  public get menus(): AIDrawMenuInfo[] {
    if (!this._data?.currKey) {
      this.setData();
    }
    return this._data?.menus || [];
  }

  public add(type: SiderMenuEnum, parent?: string): void {
    const label = (type === SiderMenuEnum.Group ? "合集" : "会话") + getUuid(5);
    const uid = getUuid();
    const item: AIDrawMenuInfo = { uid, label, type, image: { model: DrawModelEnum.Normal } as AIDrawMenuImageInfo };
    if (parent) {
      this._data?.menus?.map((val) => {
        if (val.uid === parent) {
          val.childrens = (val.childrens || []).concat(item);
        }
        return val;
      });
    } else {
      this._data = {
        currKey: uid,
        menus: (this._data?.menus || []).concat(item),
      };
    }
    this.setlocalStorage("add");
  }

  public edit(uid: string, label: string): void {
    this._data.menus?.forEach((menu) => {
      if (menu.uid === uid) menu.label = label;
      menu.childrens?.forEach((child) => {
        if (child.uid === uid) child.label = label;
      });
    });
    this.setlocalStorage("edit");
  }

  public remove(uid: string): void {
    this._data.menus = this._data.menus?.filter((val) => {
      val.childrens = val.childrens?.filter((child) => child.uid !== uid);
      return val.uid !== uid;
    });
    this._data.currKey = this._data.menus[0]?.uid || "";
    this.setlocalStorage("remove");
  }

  public setCurrKey(uid: string): void {
    this._data.currKey = uid;
    this.setlocalStorage("setCurrKey");
  }

  public get currKey(): string {
    return this._data?.currKey;
  }

  public get currMenu(): AIDrawMenuInfo {
    if (!this._data?.menus?.length) return;
    const key = this.currKey;
    for (const item of this._data?.menus) {
      if (item.uid === key) {
        return item;
      } else {
        if (item?.childrens) {
          for (const child of item?.childrens) {
            if (child.uid === key) return child;
          }
        }
      }
    }
    return null;
  }

  public updateImage(task_id: string, img: string, forward: string, exclude?: string, model: DrawModelEnum = DrawModelEnum.Normal): void {
    const uid = getUuid();
    const key = this._data.currKey;
    const image: AIDrawMenuImageInfo = { uid, task_id, img, forward, exclude, model }
    for (const item of this._data.menus) {
      if (item.uid === key) item.image = image;
      if (item?.childrens) {
        for (const child of item?.childrens) {
          if (child.uid === key) child.image = image;
        }
      }
    }
    this.setlocalStorage("updateImage");
  }

  /** 清空 */
  public clear(): void {
    this._data = {};
    this.setlocalStorage("clear");
  }

  private setlocalStorage(soure: string): void {
    console.log("setlocalStoragesetloca >>> soure :", soure, this._data);
    localStorage.setItem(this.LOCAL_STORAGE_KEY, JSON.stringify(this._data));
  }
}
