import { getUuid } from "@/utils/common.methods";
import { AessionInfo, RoleEnum, SiderMenuEnum } from "./@types";
import { Message } from "@arco-design/web-vue";

export type ChatFileMenuInfo = {
  uid: string;
  label: string;
  fileUid?: string;
  fileTime?: number;
  type: SiderMenuEnum;
  sessions?: AessionInfo[];
  childrens?: ChatFileMenuInfo[];
};

export type ChatFileInfo = {
  currKey?: string;
  menus?: ChatFileMenuInfo[];
};

export class ChatFileModel {
  private LOCAL_STORAGE_KEY = "chat-file";

  private _data = <ChatFileInfo>{};

  constructor() { }

  public setData(): void {
    const dataStr = localStorage.getItem(this.LOCAL_STORAGE_KEY);
    this._data = dataStr ? JSON.parse(dataStr) : {};
  }

  /** 左侧目录 */
  public get menus(): ChatFileMenuInfo[] {
    if (!this._data?.currKey) {
      this.setData();
    }
    return this._data?.menus || [];
  }

  public add(type: SiderMenuEnum, parent?: string): void {
    const label = (type === SiderMenuEnum.Group ? "合集" : "会话") + getUuid(5);
    const uid = getUuid();
    const item: ChatFileMenuInfo = { uid, label, type };
    if (parent) {
      this._data?.menus?.map((val) => {
        if (val.uid === parent) {
          val.childrens = (val.childrens || []).concat(item);
        }
        return val;
      });
    } else {
      this._data = {
        currKey: uid,
        menus: (this._data?.menus || []).concat(item),
      };
    }
    this.setlocalStorage("add");
  }

  public edit(uid: string, label: string): void {
    this._data.menus?.forEach((menu) => {
      if (menu.uid === uid) menu.label = label;
      menu.childrens?.forEach((child) => {
        if (child.uid === uid) child.label = label;
      });
    });
    this.setlocalStorage("edit");
  }

  public remove(uid: string): void {
    this._data.menus = this._data.menus?.filter((val) => {
      val.childrens = val.childrens?.filter((child) => child.uid !== uid);
      return val.uid !== uid;
    });
    this._data.currKey = this._data.menus[0]?.uid || "";
    this.setlocalStorage("remove");
  }

  public setCurrKey(uid: string): void {
    this._data.currKey = uid;
    this.setlocalStorage("setCurrKey");
  }

  public get currKey(): string {
    return this._data?.currKey;
  }

  public get currMenu(): ChatFileMenuInfo {
    if (!this._data?.menus?.length) return;
    const key = this.currKey;
    if (this._data?.menus) {
      for (const item of this._data?.menus) {
        if (item.uid === key) {
          return item;
        } else {
          if (item?.childrens) {
            for (const child of item?.childrens) {
              if (child.uid === key) return child;
            }
          }
        }
      }
    }

    return null;
  }

  public updatedFile(uid: string, fileUid: string) {
    for (const item of this._data.menus!) {
      if (item.uid === uid) {
        item.fileUid = fileUid;
        item.fileTime = Date.now();
      }
      if (item?.childrens) {
        for (const child of item?.childrens) {
          if (child.uid === uid) {
            child.fileUid = fileUid;
            child.fileTime = Date.now();
          }
        }
      }

    }
    this.setlocalStorage("updatedFile");
  }

  /**
   * 创建会话
   * @param role
   * @param richtext
   * @param quote
   * @returns 会话 uid
   */
  public addMenuAnswer(role: RoleEnum, richtext: string, quote?: string): string {
    const uid = getUuid();
    const key = this._data.currKey;
    const sessions: AessionInfo = { uid, role, timestamp: Date.now(), richtext, quote };
    for (const item of this._data.menus) {
      if (item.uid === key) {
        item.sessions = (item.sessions || []).concat(sessions);
      }
      if (item?.childrens) {
        for (const child of item?.childrens) {
          if (child.uid === key) {
            child.sessions = (child.sessions || []).concat(sessions);
          }
        }
      }
    }
    this.setlocalStorage("addMenuAnswer");
    return uid;
  }

  public removeMenuAnswer(uid: string, isnotice: boolean = true) {
    console.log(this._data.menus, uid);
    for (const item of this._data.menus) {
      if (item.uid === this._data.currKey) {
        item.sessions = item.sessions?.filter((val) => val.uid !== uid);
      }
      if (item?.childrens) {
        for (const child of item?.childrens) {
          if (child.uid === this._data.currKey) {
            child.sessions = child.sessions?.filter((val) => val.uid !== uid);
          }
        }
      }
    }
    if (isnotice) Message.success("删除成功");
    this.setlocalStorage("removeMenuAnswer");
  }

  /** 清空 */
  public clear(): void {
    this._data = {};
    this.setlocalStorage("clear");
  }

  private setlocalStorage(soure: string): void {
    console.log("setlocalStoragesetloca >>> soure :", soure, this._data);
    localStorage.setItem(this.LOCAL_STORAGE_KEY, JSON.stringify(this._data));
  }
}
