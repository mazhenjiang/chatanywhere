import { getUuid } from "@/utils/common.methods";
import { AessionInfo, RoleEnum, SiderMenuEnum } from "./@types";
import { Message } from "@arco-design/web-vue";

export type ChatWorkMenuInfo = {
  uid: string;
  label: string;
  fileUid?: string;
  fileTime?: number;
  type: SiderMenuEnum;
  sessions?: AessionInfo[];
  childrens?: ChatWorkMenuInfo[];
  modelapi: ModelApiEnum;
};

export type ChatWorkInfo = {
  currKey?: string;
  menus?: ChatWorkMenuInfo[];
};

export enum ModelApiEnum {
  V35 = 'v3.5',
  GLM130b = 'glm-130b',
  V40 = 'v4.0',
  claude = 'claude',
  claude100k = 'claude_100k',
  claudejia = 'claude+',
  article_polish = 'article_polish',
  article_polish_Chinese = 'article_polish_Chinese',
  Elementary_school_student = 'Elementary_school_student',
  English_teacher = 'English_teacher',
  translate_to_English = 'translate_to_English',
  translate_to_Chinese = 'translate_to_Chinese',
  xiaohongshu = 'xiaohongshu'
}

export const ModelApiUrl = {
  url1: 'https://www.gaosijiaoyu.cn/message',
  url2: 'https://www.gaosijiaoyu.cn/message_poe',
};

export const ModelApiEnumOption = [
  {
    key: ModelApiEnum.V35,
    name: 'v3.5',
    url: ModelApiUrl.url1,
    model: 'v3.5'
  },
  {
    key: ModelApiEnum.GLM130b,
    name: 'glm-130b',
    url: ModelApiUrl.url1,
    model: 'glm130b'
  },
  {
    key: ModelApiEnum.V40,
    name: 'v4.0',
    url: ModelApiUrl.url2,
    model: 'beaver'
  },
  {
    key: ModelApiEnum.claude,
    name: 'claude',
    url: ModelApiUrl.url2,
    model: 'a2'
  },
  {
    key: ModelApiEnum.claude100k,
    name: 'claude_100k',
    url: ModelApiUrl.url2,
    model: 'a2_100k'
  },
  {
    key: ModelApiEnum.claudejia,
    name: 'claude+',
    url: ModelApiUrl.url2,
    model: 'a2_2'
  },
  {
    key: ModelApiEnum.article_polish,
    name: '学术论文润色-英文',
    url: ModelApiUrl.url1,
    model: 'article_polish'
  },
  {
    key: ModelApiEnum.article_polish_Chinese,
    name: '学术论文润色-中文',
    url: ModelApiUrl.url1,
    model: 'article_polish_Chinese'
  },
  {
    key: ModelApiEnum.Elementary_school_student,
    name: '小学生解题(数学除外)',
    url: ModelApiUrl.url1,
    model: 'Elementary_school_student'
  },
  {
    key: ModelApiEnum.English_teacher,
    name: '英文解题',
    url: ModelApiUrl.url1,
    model: 'English_teacher'
  },
  {
    key: ModelApiEnum.translate_to_English,
    name: '翻译成英文',
    url: ModelApiUrl.url1,
    model: 'translate_to_English'
  },
  {
    key: ModelApiEnum.translate_to_Chinese,
    name: '翻译成中文',
    url: ModelApiUrl.url1,
    model: 'translate_to_Chinese'
  },
  {
    key: ModelApiEnum.xiaohongshu,
    name: '小红书风格文案',
    url: ModelApiUrl.url1,
    model: 'xiaohongshu'
  }
];

export class ChatWorkModel {
  private LOCAL_STORAGE_KEY = "chat-work";

  private _data = <ChatWorkInfo>{};

  constructor() {}

  public setData(): void {
    const dataStr = localStorage.getItem(this.LOCAL_STORAGE_KEY);
    this._data = dataStr ? JSON.parse(dataStr) : {};
  }

  /** 左侧目录 */
  public get menus(): ChatWorkMenuInfo[] {
    if (!this._data?.currKey) {
      this.setData();
    }
    return this._data?.menus || [];
  }

  public add(type: SiderMenuEnum, parent?: string): void {
    const label = (type === SiderMenuEnum.Group ? "合集" : "会话") + getUuid(5);
    const uid = getUuid();
    const modelapi = ModelApiEnum.V35;
    const item: ChatWorkMenuInfo = { uid, label, type, modelapi};
    if (parent) {
      this._data?.menus?.map((val) => {
        if (val.uid === parent) {
          val.childrens = (val.childrens || []).concat(item);
        }
        return val;
      });
    } else {
      this._data = {
        currKey: uid,
        menus: (this._data?.menus || []).concat(item),
      };
    }
    this.setlocalStorage("add");
  }

  public edit(uid: string, label: string): void {
    this._data.menus?.forEach((menu) => {
      if (menu.uid === uid) menu.label = label;
      menu.childrens?.forEach((child) => {
        if (child.uid === uid) child.label = label;
      });
    });
    this.setlocalStorage("edit");
  }

  public remove(uid: string): void {
    this._data.menus = this._data.menus?.filter((val) => {
      val.childrens = val.childrens?.filter((child) => child.uid !== uid);
      return val.uid !== uid;
    });
    this._data.currKey = this._data.menus[0]?.uid || "";
    this.setlocalStorage("remove");
  }

  public setCurrKey(uid: string): void {
    this._data.currKey = uid;
    this.setlocalStorage("setCurrKey");
  }

  public get currKey(): string {
    return this._data?.currKey;
  }

  public get currMenu(): ChatWorkMenuInfo {
    if (!this._data?.menus?.length) return;
    const key = this.currKey;
    for (const item of this._data?.menus) {
      if (item.uid === key) {
        return item;
      } else {
        if (item?.childrens) {
          for (const child of item?.childrens) {
            if (child.uid === key) return child;
          }
        }
      }
    }
    return null;
  }

  public updatedFile(uid: string, fileUid: string) {
    for (const item of this._data.menus!) {
      if (item.uid === uid) {
        item.fileUid = fileUid;
        item.fileTime = Date.now();
      }
      for (const child of item?.childrens) {
        if (child.uid === uid) {
          child.fileUid = fileUid;
          child.fileTime = Date.now();
        }
      }
    }
    this.setlocalStorage("updatedFile");
  }

  /**
   * 创建会话
   * @param role
   * @param richtext
   * @param quote
   * @returns 会话 uid
   */
  public addMenuAnswer(role: RoleEnum, richtext: string, quote?: string): string {
    const uid = getUuid();
    const key = this._data.currKey;
    const sessions: AessionInfo = { uid, role, timestamp: Date.now(), richtext, quote };
    for (const item of this._data.menus) {
      if (item.uid === key) {
        item.sessions = (item.sessions || []).concat(sessions);
      }
      if (item?.childrens) {
        for (const child of item?.childrens) {
          if (child.uid === key) {
            child.sessions = (child.sessions || []).concat(sessions);
          }
        }
      }
    }
    this.setlocalStorage("addMenuAnswer");
    return uid;
  }

  public removeMenuAnswer(uid: string, isnotice: boolean = true) {
    console.log(this._data.menus, uid);
    for (const item of this._data.menus) {
      if (item.uid === this._data.currKey) {
        item.sessions = item.sessions?.filter((val) => val.uid !== uid);
      }
      if (item?.childrens) {
        for (const child of item?.childrens) {
          if (child.uid === this._data.currKey) {
            child.sessions = child.sessions?.filter((val) => val.uid !== uid);
          }
        }
      }
    }
    if (isnotice) Message.success("删除成功");
    this.setlocalStorage("removeMenuAnswer");
  }

  /** 清空 */
  public clear(): void {
    this._data = {};
    this.setlocalStorage("clear");
  }

  private setlocalStorage(soure: string): void {
    console.log("setlocalStoragesetloca >>> soure :", soure, this._data);
    localStorage.setItem(this.LOCAL_STORAGE_KEY, JSON.stringify(this._data));
  }
}
