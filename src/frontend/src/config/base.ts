/** 站点配置 */
export const Site = {
  name: "在问",
  slogan: "让知识无界,智能触手可及",
  filing_number: "京ICP备2023013166号-1" || "蜀ICP备2021004261号-2" ,
  oldwebsite: "https://www.zaiwen.top/",
};
