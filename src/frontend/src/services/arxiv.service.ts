import { BaseServices } from "./base.services";
import { RoleEnum } from "@/models/@types";
import qs from "qs";

export type GeneratePromptReq = {
  role: RoleEnum;
  content: string;
};

export class ArxivService extends BaseServices {
  private static _ins: ArxivService = new ArxivService();
  public static get ins(): ArxivService {
    return this._ins ? this._ins : (this._ins = new ArxivService());
  }

  public search(query: string): Promise<string> {
    const param = qs.stringify({ start: 0, max_results: 5, search_query: query });
    const url = "https://export.arxiv.org/api/query?" + param;
    return new Promise<string>((reslove) => {
      this.get(url).then((res: string) => {
        reslove(res);
      });
    });
  }
}

export const arxivService: ArxivService = ArxivService.ins;
