import { appRef } from "@/models/app.ref";
import { BaseServices } from "./base.services";
import { Method, Uri, RespInfo, config } from "./config";
import { RoleEnum } from "@/models/@types";
import { ModelApiEnum, ModelApiEnumOption} from "@/models/chat-work";
import { stringify } from "querystring";

export class ShareService extends BaseServices {
  private static _ins: ShareService = new ShareService();
  public static get ins(): ShareService {
    return this._ins ? this._ins : (this._ins = new ShareService());
  }
  /** 发送消息 */
  public send(data :any): Promise<any> {
    const url = "https://www.13042332817.top/dataApi/share/insertData?userId=0";
    const sharedata = {
      "label": "",
      "conversation": data
    }
    return new Promise<any>((reslove, reject) => {
      this.request(url, sharedata)
        .then(async (res) => {
          console.log(res)
          reslove(res);
        })
        .catch((err) => {
          console.log(err)
          reject(err.stack);
        });
    });
  }
  public getrecords(uuid: String): Promise<string>  {
    const url = "https://www.13042332817.top/dataApi/share/getDataViaUuid?uuid=" + uuid;
    return new Promise<string>((reslove, reject) => {
      this.get(url)
        .then(async (res) => {
          console.log(res)
          reslove(JSON.stringify(res));
        })
        .catch((err) => {
          console.log(err)
          reject(err.stack);
        });
    });
  }
}
export const shareService: ShareService = ShareService.ins;