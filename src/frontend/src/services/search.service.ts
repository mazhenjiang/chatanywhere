import { appRef } from "@/models/app.ref";
import { BaseServices } from "./base.services";
export class SearchService extends BaseServices {
  private static _ins: SearchService = new SearchService();
  public static get ins(): SearchService {
    return this._ins ? this._ins : (this._ins = new SearchService());
  }

  public search(message: string, chunk: (data: string) => void, end: (data: string) => void): void {
    const url = "https://www.13042332817.top/search";
    const data = { message };
    return this.stream(url, data, chunk, end);
  }

  public message(content: string, chunk: (data: string) => void, end: (data: string) => void): void {
    const message = [{ role: "user", content }];
    const url = "https://www.gaosijiaoyu.cn/message";
    const data = { message, user_key: appRef.user.data.openai_key };
    return this.stream(url, data, chunk, end);
  }
}

export const searchService: SearchService = SearchService.ins;
