import { BaseServices } from "./base.services";
import { RoleEnum } from "@/models/@types";

export type GeneratePromptReq = {
  role: RoleEnum;
  content: string;
};

export class PromptService extends BaseServices {
  private static _ins: PromptService = new PromptService();
  public static get ins(): PromptService {
    return this._ins ? this._ins : (this._ins = new PromptService());
  }

  /**
   * 生成提示词
   * 基于消息生成的三个更多提示的接口（使用gpt3.5）
   */
  public generate_prompt(message: GeneratePromptReq[]): Promise<string> {
    const url = "https://www.gaosijiaoyu.cn/generate_prompt";
    const data = { message };
    return new Promise<string>((reslove) => {
      if (!message?.length) {
        reslove("");
      } else {
        this.request(url, data, "application/json").then((res: string) => {
          reslove(res);
        });
      }
    });
  }
}

export const promptService: PromptService = PromptService.ins;
