import { BaseServices } from "./base.services";
import { RoleEnum } from "@/models/@types";

export type GeneratePromptReq = {
  role: RoleEnum;
  content: string;
};

export class BingService extends BaseServices {
  private static _ins: BingService = new BingService();
  public static get ins(): BingService {
    return this._ins ? this._ins : (this._ins = new BingService());
  }

  public search(message: string): Promise<string> {
    const url = "https://www.gaosijiaoyu.cn/search";
    const data = { message, stop_summary: true };
    return new Promise<string>((reslove) => {
      this.request(url, data).then((res: string) => {
        reslove(res);
      });
    });
  }
}

export const bingService: BingService = BingService.ins;
