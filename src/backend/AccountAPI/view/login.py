import sqlalchemy as db
from typing import Any, List
from pydantic import BaseModel
from fastapi import APIRouter
import datetime
from core import dbSession
import uuid

from .. import models

router = APIRouter(
    prefix="/login",
    tags=["login"]
)

class LoginInfo(BaseModel):
    username:str
    password:str

session = dbSession['sqlite']

@router.post("/addLoginInfo")
async def addLoginInfo(data:LoginInfo):
    query = session.query(models.Account).filter(
        db.and_(data.username==models.Account.username,data.password==models.Account.password,models.Account.verified==True)
    )
    result:models.Account = query.first()
    if(result==None):
        return {"status":False,"message":"该账号未注册或未激活"}
    else:
        bulidDate = int(1000*datetime.datetime.now().timestamp())
        stopDate = bulidDate + 1*24*60*60*1000
        uuidString = str(uuid.uuid4())
        loginInfo = models.LoginInfo(**{
            "userId":result.id,"buildDate":bulidDate,"stopDate":stopDate,"requestNums":0,"identificationCode":uuidString
        })
        session.add(loginInfo)
        session.commit()
        return {"status":True,"message":"登录成功","loginInfo":{
            "verified":result.verified,"group":result.group,"identificationCode":uuidString
        }}