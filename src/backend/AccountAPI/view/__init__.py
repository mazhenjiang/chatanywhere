from fastapi import APIRouter
from .login import router as loginRouter
from .register import router as registerRotuer
router = APIRouter(
    prefix="/accountApi",
    tags=["accountApi"]
)

router.include_router(loginRouter)
router.include_router(registerRotuer)