import sqlalchemy as db
from sqlalchemy.ext.declarative import declarative_base
from core import dbSession

ModelBase = declarative_base()

class Account(ModelBase):
    __tablename__ = "account"
    id = db.Column(db.Integer, primary_key=True,comment="序号")
    username = db.Column(db.String(length=128),comment="用户名")
    password = db.Column(db.String(length=128),comment="密码")
    group = db.Column(db.String(length=64),comment="用户组")
    email = db.Column(db.String(length=128),comment="邮箱")
    buildDate = db.Column(db.Integer,comment="创建日期时间戳")
    verified = db.Column(db.Boolean,comment="是否验证")
    activationCode = db.Column(db.String(length=128),comment="激活码")

class LoginInfo(ModelBase):
    __tablename__ = "loginInfo"
    id = db.Column(db.Integer, primary_key=True,comment="序号")
    userId = db.Column(db.Integer, comment="用户Id")
    requestNums = db.Column(db.Integer, comment="请求次数")
    buildDate = db.Column(db.Integer,comment="创建日期时间戳")
    stopDate = db.Column(db.Integer,comment="结束日期时间戳")
    identificationCode = db.Column(db.String(length=128),comment="识别码")

session = dbSession["sqlite"]
Account.metadata.create_all(session.get_bind())