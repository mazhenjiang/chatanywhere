'''
Author: zhanwei xu
Date: 2023-06-14 16:24:32
LastEditors: zhanwei xu
LastEditTime: 2023-06-14 17:08:47
Description: 

Copyright (c) 2023 by zhanwei xu, Tsinghua University, All Rights Reserved. 
'''
from core import setting, mgClient

dbList = mgClient.list_database_names()
for dbName in ["chat"]:
    if(dbName not in dbList):
        db = mgClient[dbName]
        print(f"数据库已创建:{dbName}")