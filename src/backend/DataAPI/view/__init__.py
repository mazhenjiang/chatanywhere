from fastapi import APIRouter
from .sync import router as syncRouter
from .share import router as shareRotuer
router = APIRouter(
    prefix="/dataApi",
    tags=["dataApi"]
)

router.include_router(syncRouter)
router.include_router(shareRotuer)
