import os
# generate random key, length 32 with each character in range [a-z0-9]
import string
import random

# Define the possible characters for the key
characters = string.ascii_lowercase + string.digits

# Define the length of the key
key_length = 32

# Use the random module to generate a key with the desired length and characters
key = ''.join(random.choice(characters) for i in range(key_length))

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--num', type=int, default=1)
args = parser.parse_args()

from utils import add_user

num_token = args.num/7.3/0.0002*1000
print(num_token)

add_user(key, num_token)

print(f"key:{key}, num_token:{num_token}")
