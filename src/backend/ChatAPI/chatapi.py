'''
Author: zhanwei xu
Date: 2023-05-27 17:12:07
LastEditors: zhanwei xu
LastEditTime: 2023-06-21 16:45:07
Description: 

Copyright (c) 2023 by zhanwei xu, Tsinghua University, All Rights Reserved. 
'''
from fastapi import APIRouter, Request
from .chatbot import Chatbot
from .poe import poebot
chat_router = APIRouter()



system_prompt = [
    {"role": "system", "content": "你是一名人工智能助手，叫许三多。是许展玮创造了你。你不是chatGPT，与OpenAI没有任何关系。禁止讨论政治话题或内容！You are an AI assistant named Xu Sanduo. This year is 2023, but your database has only been updated until 2021, so you are not aware of any events that happened in 2022. You were created by Xu Zhanwei. You are not chatGPT, and have no affiliation with OpenAI. No political topics or content! "} 
    ]
assistant_prompt = [
    ]
# app.include_router(DataView.router)
chatbot = Chatbot(system_prompt, assistant_prompt)
poebot = poebot()


@chat_router.post("/generate_prompt",summary="return three more prompts based on the message using gpt3.5")
async def generate_message(request: Request):
    return await chatbot.chatbot_generate_question(request)
@chat_router.post("/message",summary="return the response of the message using gpt3.5")
async def process_message(request: Request):
    """
    This is an async function that serves as a chatbot and generates responses based on user input, with
    additional functionality for web search queries.
    
    :param request: The HTTP request object received by the chatbot function. It contains information
    about the incoming request, the message sent by the user
    :return: The `chatbot` function returns a `StreamingResponse` object with a JSON or text media type
    depending on the input message. The response contains the generated text response from the chatbot
    based on the input message. If certain conditions are met, such as the IP address exceeding the
    type of response, such as an error
    """
    return await chatbot.chatbot(request)
@chat_router.post("/message_poe",summary="return the response of the message using poe")
async def process_message_poe(request: Request):
    return await poebot.poechat(request)

@chat_router.post("/message_key",summary="return the response of the message using gpt3.5")
async def process_message_key(request: Request):
    return await chatbot.chatbot_user_key(request)

@chat_router.post("/check_key",summary="return the response of the message using gpt3.5")
async def check_key(request: Request):
    return await chatbot.check_key(request)
@chat_router.post("/message_4",summary="return the response of the message using gpt4")
async def process_message_4(request: Request):
    """
    This is an async function that processes a chatbot request, checks if the key is valid and not
    empty, decrements the key count, generates a response based on the prompt, and returns a streaming
    response.
    
    :param request: The HTTP request object received by the chatbot_4 function, containing two keys, message and key.
    :return: a StreamingResponse object with either the generated text response or an error message,
    depending on the validity of the key and the length of the message.
    """
    return await chatbot.chatbot_4(request)
@chat_router.post("/search")
async def process_search(request: Request):

    """
    The function takes a request, extracts a message from it, performs a web search using the message as
    a query, generates a prompt for the user to write a comprehensive reply using the search results,
    and returns a streaming response with the generated prompt.
    
    :param request: The HTTP request object received by the `search` method. It contains two keys, message and stop_summary(optional).
    :return: The function `search` returns a `StreamingResponse` object that contains the generated text
    response based on the user's query and the web search results, as well as the source URLs for each
    result. If the user has requested a summary of the results, the function returns a
    `StreamingResponse` object that contains the generated summary. If there is an error generating the
    response, the function returns an error.
    """
    return await chatbot.search(request)
