from fastapi import APIRouter
from .fileask import router as fileRotuer
router = APIRouter(
    prefix="/fileApi",
    tags=["fileApi"]
)

router.include_router(fileRotuer)
