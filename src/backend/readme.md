# 后端

## 数据库

数据库采用的是mongodb数据库

## 运行库

所需运行库在requirements.txt中

安装依赖命令
```
pip install -r requirements.txt
```

## 运行方法

运行命令
```
uvicorn server:app --reload --port 8000
```

## 接口文档

运行服务之后，在接口下的docs路径能够看到接口文档

如 http://localhost:8000/docs
